import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';

@NgModule({
  imports: [MatSelectModule, MatInputModule, MatButtonModule, MatFormFieldModule],
  exports: [MatSelectModule, MatInputModule, MatButtonModule, MatFormFieldModule]
})
export class MaterialAppModule { }
