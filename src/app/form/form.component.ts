import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

export interface Country {
  id: number;
  viewValue: string;
  code: string;
  flag: string;
}

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})

export class FormComponent implements OnInit {
  selected: string;

  countries: Country[] = [
    {id: 1, viewValue: 'Germany', code: '+49', flag: 'de'},
    {id: 2, viewValue: 'Russia', code: '+7', flag: 'ru'},
    {id: 3, viewValue: 'Latvia', code: '+371', flag: 'lv'},
    {id: 4, viewValue: 'France', code: '+33', flag: 'fr'},
    {id: 5, viewValue: 'Hungary', code: '+36', flag: 'hu'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
